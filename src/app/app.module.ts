import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import { firebaseConfig } from '../environments/environment';

import { AppComponent } from './app.component';
import { LoginComponent } from './Components/login/login.component';
import { HomeComponent } from './Components/home/home.component';
import { UserdataComponent } from './Components/userdata/userdata.component';
import { ProfileComponent } from './Components/profile/profile.component';
import { HeaderComponent } from './Components/header/header.component';
import { SliderComponent } from './Components/slider/slider.component';
import { NotfoundComponent } from './Components/notfound/notfound.component';
import { ServicesComponent } from './Components/services/services.component';
import { AddServiceComponent } from './Components/add-service/add-service.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'user-profile', component: ProfileComponent },
  { path: 'services', component: ServicesComponent },
  { path: 'add-service', component: AddServiceComponent },
  { path: '**', component: NotfoundComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    UserdataComponent,
    ProfileComponent,
    HeaderComponent,
    SliderComponent,
    NotfoundComponent,
    ServicesComponent,
    AddServiceComponent,
  ],
  imports: [BrowserModule, RouterModule.forRoot(routes),  AngularFireModule.initializeApp(firebaseConfig)],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
