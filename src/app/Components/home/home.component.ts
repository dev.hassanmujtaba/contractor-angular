import { Component, OnInit } from '@angular/core';
import firebase from "firebase";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  user: any;
  constructor() {}
  ngOnInit(): void {
    firebase.auth().onAuthStateChanged((user) => {
      this.user = user;
    });
  }
  logout() {
    firebase
      .auth()
      .signOut()
      .then(() => {})
      .catch((error) => {
        console.log(error);
      });
  }
}
