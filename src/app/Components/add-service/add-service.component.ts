import { Component, OnInit } from '@angular/core';
import firebase from 'firebase';
@Component({
  selector: 'app-add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.css'],
})
export class AddServiceComponent implements OnInit {
  title: string = '';
  category: string = '';
  price: string = '';
  desc: string = '';
  constructor() {}

  ngOnInit(): void {}

  addService(
    $event: any,
    title: string,
    category: string,
    price: string,
    desc: string
  ) {
    $event.preventDefault();
    let db = firebase.firestore();
    db.collection('services')
      .doc()
      .set({
        title: title,
        category: category,
        price: price,
        desc: desc,
        authorEmail: firebase.auth().currentUser?.email,
        authorName: firebase.auth().currentUser?.displayName,
        authorID: firebase.auth().currentUser?.uid,
        authorPhoto: firebase.auth().currentUser?.photoURL,
        time: firebase.firestore.FieldValue.serverTimestamp(),
      })
      .then(() => {
        this.title = '';
        this.category = '';
        this.price = '';
        this.desc = '';
        console.log('Service Data saved')
      })
      .catch((error) => {
        console.log('error', error);
      });
    console.log(title, category, price, desc);
  }
}
