import { Component, OnInit } from '@angular/core';
import firebase from 'firebase';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  user: any;
  constructor() {}

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged((user) => {
      this.user = user;
    });
  }

}
