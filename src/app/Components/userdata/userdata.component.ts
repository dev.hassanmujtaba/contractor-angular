import { Component, OnInit } from '@angular/core';
import firebase from 'firebase';

@Component({
  selector: 'app-userdata',
  templateUrl: './userdata.component.html',
  styleUrls: ['./userdata.component.css'],
})
export class UserdataComponent implements OnInit {
  user: any;

  constructor() {}

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged((user) => {
      this.user = user;
    });
  }

  getUserData($event: any, name: any, email: any, location: any, number: any) {
    $event.preventDefault();
    let db = firebase.firestore();
    db.collection('users')
      .doc(firebase.auth().currentUser?.uid)
      .set(
        {
          name: name,
          email: email,
          location: location,
          number: number,
        },
        { merge: true }
      )
      .then((docRef) => {
        console.log('User Data saved', docRef);
      })
      .catch((error) => {
        console.log('error', error);
      });
    console.log(name, email, location, number);
  }
}
