import { Component, OnInit } from '@angular/core';
import firebase from 'firebase';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  userData: any  
  user: any  
  
  private itemCollection: AngularFirestoreCollection<any> | undefined;
  services: Observable<any[]> | undefined;
  date: any
  constructor(private firestore: AngularFirestore) {
    this.itemCollection = this.firestore.collection<any>('services', ref=> {
      return ref.where("authorID", "==", firebase.auth().currentUser?.uid)
    });
    this.services = this.itemCollection.valueChanges();
  }

  ngOnInit(): void {
    firebase.firestore().collection("users")
    .doc(firebase.auth().currentUser?.uid)
    .get()
    .then((data)=>{
      if (data.exists) {
        this.userData = data.data()
        console.log("profile",data.data());
      }else{
        null
      }
    })

    firebase.auth().onAuthStateChanged((user) => {
      this.user = user;
    });
  }
}
