import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import firebase from 'firebase';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  user: any;
  constructor(private router: Router) {}

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged((user) => {
      this.user = user;
    });
  }
  logout() {
    firebase
      .auth()
      .signOut()
      .then(() => {
        this.router.navigateByUrl("/")
      })
      .catch((error) => {
        console.log(error);
      });
  }
}
