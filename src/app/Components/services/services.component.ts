import { Component, OnInit } from '@angular/core';
import firebase from 'firebase';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
})
export class ServicesComponent implements OnInit {
  private itemCollection: AngularFirestoreCollection<any> | undefined;
  services: Observable<any[]> | undefined;
  date: any
  constructor(private firestore: AngularFirestore) {
    this.itemCollection = this.firestore.collection<any>('services');
    this.services = this.itemCollection.valueChanges();  
  }

  ngOnInit(): void {}
}
