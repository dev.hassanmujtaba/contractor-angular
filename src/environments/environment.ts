// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: false
};
export const firebaseConfig = {
  apiKey: "AIzaSyCpNf5c606vCSfhpoY9ma9f06RPW0TMOz0",
  authDomain: "contractor-angular.firebaseapp.com",
  projectId: "contractor-angular",
  storageBucket: "contractor-angular.appspot.com",
  messagingSenderId: "641437809126",
  appId: "1:641437809126:web:44106206128a89f2ba7e27",
  measurementId: "G-QG0M6NBW70"
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
